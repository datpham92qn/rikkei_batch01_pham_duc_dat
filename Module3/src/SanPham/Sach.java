package SanPham;


import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;

public class Sach extends SanPham {


    public Sach(String maSanpham, String tenSanpham, String thuocDanhmuc, int soLuong, float donGia) {
        super(maSanpham, tenSanpham, thuocDanhmuc, soLuong, donGia);
    }

    public String getNhaXuatban() {
        return nhaXuatban;
    }

    public void setNhaXuatban(String nhaXuatban) {
        this.nhaXuatban = nhaXuatban;
    }

    public String getNamXuatban() {
        return namXuatban;
    }

    public void setNamXuatban(String namXuatban) {
        this.namXuatban = namXuatban;
    }

    public String getTacGia() {
        return tacGia;
    }

    public void setTacGia(String tacGia) {
        this.tacGia = tacGia;
    }

    public String getNgayXuatban() {
        return ngayXuatban;
    }

    public void setNgayXuatban(String ngayXuatban) {
        this.ngayXuatban = ngayXuatban;
    }

    public int getSoLantaiban() {
        return soLantaiban;
    }

    public void setSoLantaiban(int soLantaiban) {
        this.soLantaiban = soLantaiban;
    }

    private String nhaXuatban, namXuatban, tacGia;
    private String ngayXuatban;
    private int soLantaiban;
    public Sach(){

    }
    public Sach(String maSanpham, String tenSanpham, String thuocDanhmuc, int soLuong, float donGia, String nhaXuatban, String namXuatban, String tacGia, String ngayXuatban, int soLantaiban) {
        super(maSanpham, tenSanpham, thuocDanhmuc, soLuong, donGia);
        this.nhaXuatban = nhaXuatban;
        this.namXuatban = namXuatban;
        this.tacGia = tacGia;
        this.ngayXuatban = ngayXuatban;
        this.soLantaiban = soLantaiban;
    }

    @Override
    public void Hienthithongtin() {
        System.out.println("++++++++++++Nha Sach Nha Nam++++++++++++++");
        System.out.println("Ma Sach: " + getMaSanpham() + "");
        System.out.println(":Ten: " + getTenSanpham() + "");
        System.out.println("Danh Muc: " + getThuocDanhmuc() + "");
        System.out.println("So luong: " + getDonGia() + "");
        System.out.println("Nha Xuat Ban: " + getNhaXuatban() + "");
        System.out.println("Nam Xuat Ban: " + getNamXuatban() + "");
        System.out.println("Tac Gia: " + getTacGia() + "");
        System.out.println("Ngay Xuat ban: " + getNgayXuatban() + "");
        System.out.println("So lan Tai ban: " + getSoLantaiban() + "");
    }

    @Override
    public void Phantramgiamgia() {
        String loaikhachhang = "";
        System.out.println(loaikhachhang == "Khachhang" ? 0.1 : loaikhachhang == "VIP1" ? 0.05 : 0.1);
    }

    public static Scanner scanner = new Scanner(System.in);
    private List<Sach> saches;
    String masp, ten, danhmuc, nhaxb, namxb, tacgia, ngayxb;
    int solanxb, soluong;
    float dongia;
    public Sach addSach() throws IOException {
        System.out.println("++++++++++++Nha Sach Nha Nam++++++++++++++");
        System.out.println("Ma Sach: ");
        masp = scanner.nextLine ();
        System.out.println ( "Ten:" );
        ten = scanner.nextLine ();
        System.out.println ( "Danh muc: " );
        danhmuc = scanner.nextLine ();
        System.out.println ( "Soluong: " );
        danhmuc = scanner.nextLine ();
        System.out.println ( "Don gia: " );
        dongia = parseFloat ( scanner.nextLine () );
        System.out.println("Nha Xuat Ban: ");
         nhaxb = scanner.nextLine();
        System.out.println("Nam Xuat Ban: ");
         namxb = scanner.nextLine();
        System.out.println("Tac Gia: ");
         tacgia = scanner.nextLine();
        System.out.println("Ngay Xuat ban: ");
         ngayxb = scanner.nextLine();
        System.out.println("So lan Tai ban: ");
         solanxb = parseInt(String.valueOf(scanner.nextInt()));
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date = formatter.parse(ngayxb);
            System.out.println(date);
            System.out.println(formatter.format(date));
        } catch (ParseException e) {
            e.printStackTrace();

        }
        String Hienthi = "Ma:"+masp+" Ten:"+ten+"So Luong:"+soluong+"Don gia:"+dongia+"NhaXb:"+nhaxb+"NamXb:"+namxb+"Tac Gia:"+tacgia+
                "NgayXb:"+ngayxb+"So Lan:"+solanxb+"";
        Sach sach = new Sach(masp, ten, danhmuc, soluong, dongia, nhaxb, namxb, tacgia, ngayxb, solanxb);
        LuuFILE luu = new LuuFILE();
        String path ="E:\\Project_Me\\Rikkei_Batch01_Pham_Duc_Dat\\Module3\\src\\File\\Sach.csv";
        luu.writeFile(path, Hienthi);
        return sach;
    }




}
