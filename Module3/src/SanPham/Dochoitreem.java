package SanPham;

import DanhMuc.Danhmuc;

import java.util.Scanner;

import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;

public class Dochoitreem extends SanPham{
    public Dochoitreem(String maSanpham, String tenSanpham, String thuocDanhmuc, int soLuong, float donGia, String xuatSu, String thuongHieu, String nhaCungcap, String huongDansudung) {
        super(maSanpham, tenSanpham, thuocDanhmuc, soLuong, donGia);
        this.xuatSu = xuatSu;
        this.thuongHieu = thuongHieu;
        this.nhaCungcap = nhaCungcap;
        this.huongDansudung = huongDansudung;
    }
    public  Dochoitreem(){

    }
    public String getXuatSu() {
        return xuatSu;
    }

    public void setXuatSu(String xuatSu) {
        this.xuatSu = xuatSu;
    }

    public String getThuongHieu() {
        return thuongHieu;
    }

    public void setThuongHieu(String thuongHieu) {
        this.thuongHieu = thuongHieu;
    }

    public String getNhaCungcap() {
        return nhaCungcap;
    }

    public void setNhaCungcap(String nhaCungcap) {
        this.nhaCungcap = nhaCungcap;
    }

    public String getHuongDansudung() {
        return huongDansudung;
    }

    public void setHuongDansudung(String huongDansudung) {
        this.huongDansudung = huongDansudung;
    }

    private String xuatSu, thuongHieu, nhaCungcap, huongDansudung;

    @Override
    public void Phantramgiamgia() {
        String loaikhachhang = "" ;
        System.out.println(loaikhachhang == "Khachhang" ? 0.02 : loaikhachhang == "VIP1" ? 0.05 : 0.07);
    }

    @Override
    public void Hienthithongtin() {
        System.out.println("++++++++++++Nha Sach Nha Nam++++++++++++++");
        System.out.println("Ma : " + getMaSanpham() + "");
        System.out.println("Ten: " + getTenSanpham() + "");
        System.out.println("Danh Muc: " + getThuocDanhmuc() + "");
        System.out.println("So luong: " + getDonGia() + "");
        System.out.println("Nha Xuat Ban: " + getXuatSu() + "");
        System.out.println("Nam Xuat Ban: " + getThuongHieu() + "");
        System.out.println("Tac Gia: " + getNhaCungcap() + "");
        System.out.println("Ngay Xuat ban: " + getHuongDansudung() + "");
    }
    public Dochoitreem addDochoi(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("++++++++++++Nha Sach Nha Nam++++++++++++++");
        System.out.println("Ma : ");
        String masp = scanner.nextLine();
        System.out.println(":Ten: ");
        String ten = scanner.nextLine();
        System.out.println("Danh Muc:");
        String danhmuc = scanner.nextLine();
        System.out.println("So luong: ");
        int soluong = parseInt(String.valueOf(scanner.nextInt()));
        System.out.println("Don Gia: ");
        float dongia = parseFloat(String.valueOf(scanner.nextFloat()));
        System.out.println("Xuat Su: ");
        String xuatxu = scanner.nextLine();
        System.out.println("Thuong Hieu: ");
        String thuonghieu = scanner.nextLine();
        System.out.println("Nha Cung Cap: ");
        String nhacungcap = scanner.nextLine();
        System.out.println("Huong dan su dung: ");
        Dochoitreem dochoitreem = new Dochoitreem(masp, ten, danhmuc,soluong,dongia, xuatSu,thuongHieu,nhaCungcap,huongDansudung);
        return dochoitreem;

    }
}
