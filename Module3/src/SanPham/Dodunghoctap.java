package SanPham;

import DanhMuc.Danhmuc;
import SanPham.SanPham;

import java.util.Scanner;

import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;

public class Dodunghoctap extends  SanPham {

    public Dodunghoctap(String maSanpham, String tenSanpham, String thuocDanhmuc, int soLuong, float donGia, String xuatSu, String thuongHieu, String nhaCungcap, String huongDansudung, String chatLieu, String kichThuoc) {
        super(maSanpham, tenSanpham, thuocDanhmuc, soLuong, donGia);
        this.xuatSu = xuatSu;
        this.thuongHieu = thuongHieu;
        this.nhaCungcap = nhaCungcap;
        this.huongDansudung = huongDansudung;
        this.chatLieu = chatLieu;
        this.kichThuoc = kichThuoc;
    }

    public Dodunghoctap(String maSanpham, String tenSanpham, String thuocDanhmuc, int soLuong, float donGia) {
        super(maSanpham, tenSanpham, thuocDanhmuc, soLuong, donGia);
    }

    public String getXuatSu() {
        return xuatSu;
    }

    public void setXuatSu(String xuatSu) {
        this.xuatSu = xuatSu;
    }

    public String getThuongHieu() {
        return thuongHieu;
    }

    public void setThuongHieu(String thuongHieu) {
        this.thuongHieu = thuongHieu;
    }

    public String getNhaCungcap() {
        return nhaCungcap;
    }

    public void setNhaCungcap(String nhaCungcap) {
        this.nhaCungcap = nhaCungcap;
    }

    public String getHuongDansudung() {
        return huongDansudung;
    }

    public void setHuongDansudung(String huongDansudung) {
        this.huongDansudung = huongDansudung;
    }

    public String getChatLieu() {
        return chatLieu;
    }

    public void setChatLieu(String chatLieu) {
        this.chatLieu = chatLieu;
    }

    public String getKichThuoc() {
        return kichThuoc;
    }

    public void setKichThuoc(String kichThuoc) {
        this.kichThuoc = kichThuoc;
    }

    private String xuatSu, thuongHieu, nhaCungcap, huongDansudung, chatLieu, kichThuoc;

    @Override
    public void Hienthithongtin() {
        System.out.println("++++++++++++Nha Sach Nha Nam++++++++++++++");
        System.out.println("Ma : " + getMaSanpham() + "");
        System.out.println("Ten: " + getTenSanpham() + "");
        System.out.println("Danh Muc: " + getThuocDanhmuc() + "");
        System.out.println("So luong: " + getDonGia() + "");
        System.out.println("Nha Xuat Ban: " + getXuatSu() + "");
        System.out.println("Nam Xuat Ban: " + getThuongHieu() + "");
        System.out.println("Tac Gia: " + getNhaCungcap() + "");
        System.out.println("Ngay Xuat ban: " + getHuongDansudung() + "");
        System.out.println("So lan Tai ban: " + getChatLieu() + "");
        System.out.println("Kich Thuoc: "+getKichThuoc()+"");
    }
    public  Dodunghoctap(){

    }
    @Override
    public void Phantramgiamgia() {
        String loaikhachhang = "" ;
        System.out.println(loaikhachhang == "Khachhang" ? 0.01 : loaikhachhang == "VIP1" ? 0.03 : 0.07);
    }
    public  Dodunghoctap addDodung(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("++++++++++++Nha Sach Nha Nam++++++++++++++");
        System.out.println("Ma : ");
        String masp = scanner.nextLine();
        System.out.println(":Ten: ");
        String ten = scanner.nextLine();
        System.out.println("Danh Muc:");
        String danhmuc = scanner.nextLine();
        System.out.println("So luong: ");
        int soluong = parseInt(String.valueOf(scanner.nextInt()));
        System.out.println("Don Gia: ");
        float dongia = parseFloat(String.valueOf(scanner.nextFloat()));
        System.out.println("Xuat Su: ");
        String xuatxu = scanner.nextLine();
        System.out.println("Thuong Hieu: ");
        String thuonghieu = scanner.nextLine();
        System.out.println("Nha Cung Cap: ");
        String nhacungcap = scanner.nextLine();
        System.out.println("Huong dan su dung: ");
        String huongdan = scanner.nextLine();
        System.out.println("Chat lieu: ");
        String chatlieu = scanner.nextLine();
        System.out.println("kich thuoc: ");
        String kichthuoc = scanner.nextLine();
        Dodunghoctap dodunghoctap = new Dodunghoctap(masp,ten,danhmuc,soluong,dongia,xuatSu,thuongHieu, nhaCungcap, huongDansudung, chatLieu, kichThuoc);
        return dodunghoctap;
    }


}
