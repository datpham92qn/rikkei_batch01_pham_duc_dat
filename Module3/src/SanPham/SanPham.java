package SanPham;

import java.util.ArrayList;

public abstract class SanPham {

    public SanPham(String maSanpham, String tenSanpham, String thuocDanhmuc, int soLuong, float donGia) {
        this.maSanpham = maSanpham;
        this.tenSanpham = tenSanpham;
        this.thuocDanhmuc = thuocDanhmuc;
        this.soLuong = soLuong;
        this.donGia = donGia;
    }
    public SanPham(){}
    public String getMaSanpham() {
        return maSanpham;
    }

    public char[] setMaSanpham() {
        this.maSanpham = maSanpham;
        return new char[0];
    }

    public String getTenSanpham() {
        return tenSanpham;
    }

    public void setTenSanpham(String tenSanpham) {
        this.tenSanpham = tenSanpham;
    }

    public String getThuocDanhmuc() {
        return thuocDanhmuc;
    }

    public void setThuocDanhmuc(String thuocDanhmuc) {
        this.thuocDanhmuc = thuocDanhmuc;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public float getDonGia() {
        return donGia;
    }

    public void setDonGia(float donGia) {
        this.donGia = donGia;
    }

    ArrayList<SanPham> sanPhams = new ArrayList<SanPham>();
    SanPham sanPham;

    private String maSanpham, tenSanpham;
    String thuocDanhmuc;
    private int soLuong;
    private  float donGia;
    public abstract void Hienthithongtin();
    public  abstract void Phantramgiamgia();


}
