package KhachHang;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;

public class Hoadon {
    public Hoadon(String maHoadon, String maKhachhang, int soLuong, String ngayMua, float donGia, double tongTien) {
        this.maHoadon = maHoadon;
        this.maKhachhang = maKhachhang;
        this.soLuong = soLuong;
        this.ngayMua = ngayMua;
        this.donGia = donGia;
        this.tongTien = tongTien;
    }
    public  Hoadon(){

    }
    private String maHoadon, maKhachhang;

    public String getNgayMua() {
        return ngayMua;
    }

    public void setNgayMua(String ngayMua) {
        this.ngayMua = ngayMua;
    }

    public String getMaHoadon() {
        return maHoadon;
    }

    public void setMaHoadon(String maHoadon) {
        this.maHoadon = maHoadon;
    }

    public String getMaKhachhang() {
        return maKhachhang;
    }

    public void setMaKhachhang(String maKhachhang) {
        this.maKhachhang = maKhachhang;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }



    public float getDonGia() {
        return donGia;
    }

    public void setDonGia(float donGia) {
        this.donGia = donGia;
    }

    public double getTongTien() {
        return tongTien;
    }

    public void setTongTien(double tongTien) {
        this.tongTien = tongTien;
    }

    private int soLuong;
    private String ngayMua;
    private float donGia;
    private double tongTien;
    public  Hoadon addHoadon(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("++++++++++++Hoa Don++++++++++++");
        System.out.println("Ma: ");
        String ma = scanner.nextLine();
        System.out.println("Ma Khach Hang: ");
        String makh = scanner.nextLine();
        System.out.println("So Luong");
        int soluong = parseInt(String.valueOf(scanner.nextInt()));
        System.out.println("Ngay Mua");
        String ngaymua = scanner.nextLine();
        System.out.println("Dong gia: ");
        float dongia = parseFloat(scanner.nextLine());
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date = formatter.parse(ngaymua);
            System.out.println(date);
            System.out.println(formatter.format(date));
        } catch (ParseException e) {
            e.printStackTrace();

        }
        tongTien = soluong * dongia;
        Hoadon hoadon = new Hoadon(ma,makh,soluong,ngaymua,dongia,tongTien);
        return hoadon;
    }

    public void HienThithongtin(){
        System.out.println("Ma:"+getMaHoadon()+" ");
        System.out.println("Ma KH:"+getMaKhachhang()+" ");
        System.out.println("So luong:"+getSoLuong()+" ");
        System.out.println("Don gia:"+getDonGia()+" ");
        System.out.println("Ngay mua:"+getNgayMua()+" ");
        System.out.println("Tong Tien:"+tongTien+" ");
    }
    public  void seachhoadon(){

    }

}
