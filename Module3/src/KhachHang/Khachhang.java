package KhachHang;

public class Khachhang {

    private String maKhachhang;
    private String hoTen;

    public String getMaKhachhang() {
        return maKhachhang;
    }

    public void setMaKhachhang(String maKhachhang) {
        this.maKhachhang = maKhachhang;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public String getSdt() {
        return Sdt;
    }

    public void setSdt(String sdt) {
        Sdt = sdt;
    }

    public String getEmaiL() {
        return emaiL;
    }

    public void setEmaiL(String emaiL) {
        this.emaiL = emaiL;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getLoaiKhachhang() {
        return loaiKhachhang;
    }

    public void setLoaiKhachhang(String loaiKhachhang) {
        this.loaiKhachhang = loaiKhachhang;
    }

    private String Sdt;
    private String emaiL;
    private String ngaySinh;
    private String loaiKhachhang;

    public Khachhang(String maKhachhang, String hoTen, String sdt, String emaiL, String ngaySinh, String loaiKhachhang) {
        this.maKhachhang = maKhachhang;
        this.hoTen = hoTen;
        Sdt = sdt;
        this.emaiL = emaiL;
        this.ngaySinh = ngaySinh;
        this.loaiKhachhang = loaiKhachhang;
    }
}
