import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AddCustomerComponent } from './addcustomer/add-customer.component';
import {FormsModule} from '@angular/forms';
import { IndexCustomersComponent } from './indexcustomers/index-customers.component';
import { AddEmployeesComponent } from './addemployees/add-employees.component';
import {RouterModule} from '@angular/router';
import {routing} from './routingconfig.module';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AddCustomerComponent,
    IndexCustomersComponent,
    AddEmployeesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
