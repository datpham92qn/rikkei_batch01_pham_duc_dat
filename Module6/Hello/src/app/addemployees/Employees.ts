export class Class {
  constructor(id: number, name: string, position: string, add: string, email: string, birthday: string, phone: string) {
    this.id = id;
    this.name = name;
    this.position = position;
    this.add = add;
    this.email = email;
    this.birthday = birthday;
    this.phone = phone;
  }
  id: number;
  name: string;
  position: string;
  add: string;
  email: string;
  birthday: string;
  phone: string;
}
