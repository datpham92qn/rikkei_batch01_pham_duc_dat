import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-add-employees',
  templateUrl: './add-employees.component.html',
  styleUrls: ['./add-employees.component.css']
})
export class AddEmployeesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(createForm: NgForm) {
    console.log(createForm.value);
  }
}
