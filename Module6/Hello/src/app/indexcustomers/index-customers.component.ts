import { Component, OnInit } from '@angular/core';
import {Customers} from '../addcustomer/Customers';

@Component({
  selector: 'app-index-customers',
  templateUrl: 'addcustomers',
  styleUrls: ['./indexcustomers.component.css']
})
export class IndexCustomersComponent implements OnInit {
  customer: Customers[] = [
    {id: 1, name: 'Le Van A', add: 'Quang Nam', email: '1@1.com', birthday: '2/2/2', phone: '01231232'},
    {id: 2, name: 'Le Van 5', add: 'Quang Tri', email: '1@1.com', birthday: '2/2/2', phone: '01231232'},
    {id: 3, name: 'Le Van 6', add: 'Phuong Nam', email: '1@1.com', birthday: '2/2/2', phone: '01231232'},
    {id: 4, name: 'Le Van Luyen', add: 'Ha Nam', email: '1@1.com', birthday: '2/2/2', phone: '01231232'},
    {id: 5, name: 'Le Van C', add: 'Binh Phuoc', email: '1@1.com', birthday: '2/2/2', phone: '01231232'}
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
