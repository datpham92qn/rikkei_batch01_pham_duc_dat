import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {AddCustomerComponent} from './addcustomer/add-customer.component';

export const  routing: Routes = [
  {path: '/', component: AppComponent},
    {path: 'addcustomer', component: AddCustomerComponent}

];

export const  appRoutingModule = RouterModule.forRoot(routing);
