package ga2.example.mrdatmyproject.Model;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table
public class Khachhang {
    @Id
    @SequenceGenerator (
            name="Khachang_sequence",
            sequenceName="Khachang_sequence",
            allocationSize=1
    )
    @GeneratedValue(
            strategy=GenerationType.IDENTITY,
            generator="Khachang_sequence"
    )
    private  Long id;
    private  String ten;
    private Date ngaysinh;
    private  String sdt;
    private  String email;
    private  Long id_loaikh;

    public Khachhang(Long id, String ten, Date ngaysinh, String sdt, String email, Long id_loaikh) {
        this.id=id;
        this.ten=ten;
        this.ngaysinh=ngaysinh;
        this.sdt=sdt;
        this.email=email;
        this.id_loaikh=id_loaikh;
    }
    public  Khachhang(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id=id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten=ten;
    }

    public Date getNgaysinh() {
        return ngaysinh;
    }

    public void setNgaysinh(Date ngaysinh) {
        this.ngaysinh=ngaysinh;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt=sdt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email=email;
    }

    public Long getId_loaikh() {
        return id_loaikh;
    }

    public void setId_loaikh(Long id_loaikh) {
        this.id_loaikh=id_loaikh;
    }

    @Override
    public String toString() {
        return "Khachhang{" +
                "id=" + id +
                ", ten='" + ten + '\'' +
                ", ngaysinh=" + ngaysinh +
                ", sdt='" + sdt + '\'' +
                ", email='" + email + '\'' +
                ", id_loaikh=" + id_loaikh +
                '}';
    }
}
