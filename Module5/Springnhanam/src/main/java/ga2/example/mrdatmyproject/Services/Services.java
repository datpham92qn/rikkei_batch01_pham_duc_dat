package ga2.example.mrdatmyproject.Services;

import ga2.example.mrdatmyproject.Model.Khachhang;

import java.util.ArrayList;

public interface Services {
    ArrayList<Khachhang> findAll();

    void save(Khachhang kh);

    Khachhang findByID(int id);

    void update(int id,Khachhang kh);

    void remote(int id);

}
