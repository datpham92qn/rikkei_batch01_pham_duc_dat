package ga2.example.mrdatmyproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MrdatmyprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run ( MrdatmyprojectApplication.class, args );
    }

}
