package ga2.example.mrdatmyproject.Services;

import ga2.example.mrdatmyproject.Model.Khachhang;

import java.util.ArrayList;

public class KhachhangImp implements Services{
    private static ArrayList<Khachhang> khang;
    @Override
    public ArrayList<Khachhang> findAll() {
        return new ArrayList<>(khang);
    }

    @Override
    public void save(Khachhang kh) {
        khang.add(kh);

    }

    @Override
    public Khachhang findByID(int id) {
        return khang.get(id);
    }

    @Override
    public void update(int id, Khachhang kh) {
        khang.add(id,kh);

    }

    @Override
    public void remote(int id) {
        khang.remove(id);

    }
}
