package ga2.example.mrdatmyproject.Controller;

import ga2.example.mrdatmyproject.Model.Khachhang;
import ga2.example.mrdatmyproject.Services.KhachhangImp;
import ga2.example.mrdatmyproject.Services.Services;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class KhachhangController {
    private Services  services= new KhachhangImp ();

    @GetMapping("/index")
    public String index(Model model, RedirectAttributes redirec)
    {
        model.addAttribute("khang",services.findAll());
        redirec.addFlashAttribute("success","");
        return "index";
    }
    @GetMapping("/create")
    public String create(Model model)
    {
        model.addAttribute("kh",new Khachhang ());
        return "create";
    }
}