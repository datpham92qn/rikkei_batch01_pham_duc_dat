/*Nha Sach nha nam DB*/

CREATE TABLE DanhMuc_Sach (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    theloai VARCHAR(50),
    mota VARCHAR(500)
);
CREATE TABLE DanhMuc_DoChoi (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nhom VARCHAR(50),
    mota VARCHAR(500)
);
CREATE TABLE DanhMuc_DungCu (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    khoi VARCHAR(50),
    mota VARCHAR(500)
);

CREATE TABLE Sanpham_Sach (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tensp VARCHAR(50),
    soluong INT,
    dongia FLOAT,
    donvi VARCHAR(50),
    id_danhmucS BIGINT,
    nhaxb VARCHAR(50),
    namxb VARCHAR(10),
    tacgia VARCHAR(50),
    ngayxb DATETIME,
    solantb INT,
	Constraint FK_danhmucsach foreign key (id_danhmucS) references DanhMuc_Sach(id)
);
CREATE TABLE Sanpham_Dochoi (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tensp VARCHAR(50),
    soluong INT,
    dongia FLOAT,
    donvi VARCHAR(50),
    id_danhmucDC BIGINT,
    xuatxu VARCHAR(150),
    thuonghieu VARCHAR(150),
    nhacungcap VARCHAR(150),
    huongdan VARCHAR(500),
    Constraint FK_danhmucdochoi foreign key (id_danhmucDC) references danhmuc_dochoi(id)
);
CREATE TABLE Sanpham_Dodung (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    tensp VARCHAR(50),
    soluong INT,
    dongia FLOAT,
    donvi VARCHAR(50),
    id_danhmucDD BIGINT,
    xuatxu VARCHAR(150),
    thuonghieu VARCHAR(150),
    nhacungcap VARCHAR(150),
    mausac VARCHAR(20),
    kichthuoc VARCHAR(50),
    chatlieu VARCHAR(50),
    huongdan VARCHAR(500),
    Constraint FK_danhmucdungcu foreign key (id_danhmucDD) references danhmuc_dungcu(id)
);
CREATE TABLE Nhanvien (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    ten VARCHAR(50),
    ngaysinh DATETIME,
    vitri VARCHAR(50),
    sdt VARCHAR(20),
    email VARCHAR(150),
    diachi VARCHAR(200)
);
CREATE TABLE LoaiKh (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    ten VARCHAR(50)
);
CREATE TABLE Khachhang (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    ten VARCHAR(50),
    ngaysinh DATETIME,
    sdt VARCHAR(20),
    email VARCHAR(150),
    id_loaikh BIGINT,
    Constraint FK_loaikh foreign key (id_loaikh) references loaikh(id)
);
CREATE TABLE LoaiKh (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    ten VARCHAR(50)
);
CREATE TABLE Donhang (
    id BIGINT not null auto_increment primary key,
    id_kh BIGINT,
    id_nv BIGINT,
    ngaymua DATETIME,
    tongtien DECIMAL(18 , 4 ),
   Constraint FK_kh foreign key (id_kh) references khachhang(id),
   Constraint FK_nv foreign key (id_nv) references nhanvien(id) 
);
CREATE TABLE ChitietHD (
    id_donhang BIGINT,
    id_sanpham BIGINT,
    soluong INT,
	 Constraint FK_donhang foreign key (id_donhang) references donhang(id),
	Constraint FK_sp_sach foreign key (id_sanpham) references sanpham_sach(id),
	Constraint FK_sp_dochoi foreign key (id_sanpham) references sanpham_dochoi(id),
     Constraint FK_sp_dungcu foreign key (id_sanpham) references sanpham_dodung(id)
);
ALTER TABLE danhmuc_sach AUTO_INCREMENT = 1;
ALTER TABLE danhmuc_dochoi AUTO_INCREMENT = 1;
ALTER TABLE sanpham_dodung AUTO_INCREMENT = 1;
ALTER TABLE sanpham_sach  modify column ngayxb date;
-- Insert table--
-- Table Danh Muc Sach--
INSERT INTO `db_nn`.`danhmuc_sach` (`id`, `theloai`, `mota`) VALUES ('1', 'Sách hay', 'hay quá');
INSERT INTO `db_nn`.`danhmuc_sach` (`id`, `theloai`, `mota`) VALUES ('2', 'Sách phim', 'hay lắm');
INSERT INTO `db_nn`.`danhmuc_sach` (`id`, `theloai`, `mota`) VALUES ('3', 'Sách kinh dị', 'sơ lắm');
INSERT INTO `db_nn`.`danhmuc_sach` (`id`, `theloai`, `mota`) VALUES ('4', 'Sách yêu', 'yêu thương');
-- Table Danh Muc  danhmuc_dungcu--
INSERT INTO `db_nn`.`danhmuc_dungcu` (`id`, `khoi`, `mota`) VALUES ('1', 'Khối 1', 'khối 1');
INSERT INTO `db_nn`.`danhmuc_dungcu` (`id`, `khoi`, `mota`) VALUES ('2', 'khối 2', 'khối 2');
INSERT INTO `db_nn`.`danhmuc_dungcu` (`id`, `khoi`, `mota`) VALUES ('3', 'khôi 3', 'khói 3');
INSERT INTO `db_nn`.`danhmuc_dungcu` (`id`, `khoi`, `mota`) VALUES ('4', 'Khối 4', 'khối 4');
-- Table Danh Muc do choi--
INSERT INTO `db_nn`.`danhmuc_dochoi` (`id`, `nhom`, `mota`) VALUES ('1', 'Dc1', 'dc1');
INSERT INTO `db_nn`.`danhmuc_dochoi` (`id`, `nhom`, `mota`) VALUES ('2', 'Dc2', 'dc2');
INSERT INTO `db_nn`.`danhmuc_dochoi` (`id`, `nhom`, `mota`) VALUES ('3', 'Dc3', 'dc3');
INSERT INTO `db_nn`.`danhmuc_dochoi` (`id`, `nhom`, `mota`) VALUES ('4', 'Dc4', 'dc4');
-- Table  do choi--
INSERT INTO `db_nn`.`sanpham_dochoi` (`id`, `tensp`, `soluong`, `dongia`, `donvi`, `id_danhmucDC`, `xuatxu`, `thuonghieu`, `nhacungcap`, `huongdan`) VALUES ('1', 'dc1', '20', '12000', 'VND', '1', 'vn', 'vn', 'vn', 'vn');
INSERT INTO `db_nn`.`sanpham_dochoi` (`id`, `tensp`, `soluong`, `dongia`, `donvi`, `id_danhmucDC`, `xuatxu`, `thuonghieu`, `nhacungcap`, `huongdan`) VALUES ('2', 'dc2', '2', '13000', 'JP', '2', 'jp', 'jp', 'jp', 'jp');
INSERT INTO `db_nn`.`sanpham_dochoi` (`id`, `tensp`, `soluong`, `dongia`, `donvi`, `id_danhmucDC`, `xuatxu`, `thuonghieu`, `nhacungcap`, `huongdan`) VALUES ('3', 'dc3', '52', '40000', 'CN', '3', 'cn', 'cn', 'cn', 'cn');
INSERT INTO `db_nn`.`sanpham_dochoi` (`id`, `tensp`, `soluong`, `dongia`, `donvi`, `id_danhmucDC`, `xuatxu`, `thuonghieu`, `nhacungcap`, `huongdan`) VALUES ('4', 'dc4', '42', '32000', 'KR', '3', 'kr', 'kr', 'kr', 'kr');
-- Table  Sach--
INSERT INTO `db_nn`.`sanpham_sach` (`id`, `tensp`, `soluong`, `dongia`, `donvi`, `id_danhmucS`, `nhaxb`, `namxb`, `tacgia`, `ngayxb`, `solantb`) VALUES ('1', 's1', '1', '22322', 'v', '1', 'vn', 'vn', 'vn', '2021/2/2', '2');
INSERT INTO `db_nn`.`sanpham_sach` (`id`, `tensp`, `soluong`, `dongia`, `donvi`, `id_danhmucS`, `nhaxb`, `namxb`, `tacgia`, `ngayxb`, `solantb`) VALUES ('2', 's2', '24', '11111', 'j', '2', 'j', 'j', 'j', '2021-02-01', '4');
INSERT INTO `db_nn`.`sanpham_sach` (`id`, `tensp`, `soluong`, `dongia`, `donvi`, `id_danhmucS`, `nhaxb`, `namxb`, `tacgia`, `ngayxb`, `solantb`) VALUES ('3', 's3', '12', '55555', '2j', '3', '2', '2', '2', '2021-12-11', '5');
-- Table Nhan Vien--
INSERT INTO `db_nn`.`nhanvien` (`id`, `ten`, `ngaysinh`, `vitri`, `sdt`, `email`, `diachi`) VALUES ('1', 'nv01', '1/1/1', 'ms', '033', 'dat@', 'quang nam');
INSERT INTO `db_nn`.`nhanvien` (`id`, `ten`, `ngaysinh`, `vitri`, `sdt`, `email`, `diachi`) VALUES ('2', 'nv02', '1/2/2', 'pm', '022', 'dat2@', 'quang tri');
INSERT INTO `db_nn`.`nhanvien` (`id`, `ten`, `ngaysinh`, `vitri`, `sdt`, `email`, `diachi`) VALUES ('3', 'nv03', '1/1/1', 'sr', '222', 'dat3@', 'quang ngai');
-- Table Khach Hang--
INSERT INTO `db_nn`.`khachhang` (`id`, `ten`, `ngaysinh`, `sdt`, `email`, `id_loaikh`) VALUES ('1', 'kh01', '1/1/1', '222', '222@', '1');
INSERT INTO `db_nn`.`khachhang` (`id`, `ten`, `ngaysinh`, `sdt`, `email`, `id_loaikh`) VALUES ('2', 'kh02', '1/2/1', '333', '333@', '3');
INSERT INTO `db_nn`.`khachhang` (`id`, `ten`, `ngaysinh`, `sdt`, `email`, `id_loaikh`) VALUES ('3', 'kh03', '1/1/2', '444', '444#', '2');

-- Table Loai KH--
INSERT INTO `db_nn`.`loaikh` (`id`, `ten`) VALUES ('1', 'VIP1');
INSERT INTO `db_nn`.`loaikh` (`id`, `ten`) VALUES ('2', 'VIP2');
INSERT INTO `db_nn`.`loaikh` (`id`, `ten`) VALUES ('3', 'BT');
-- Table Don hang--
INSERT INTO `db_nn`.`donhang` (`id`, `id_kh`, `id_nv`, `ngaymua`, `tongtien`) VALUES ('1', '1', '1', '2021/1/1', '50000');
INSERT INTO `db_nn`.`donhang` (`id`, `id_kh`, `id_nv`, `ngaymua`, `tongtien`) VALUES ('2', '2', '2', '2021/2/2', '34444');
INSERT INTO `db_nn`.`donhang` (`id`, `id_kh`, `id_nv`, `ngaymua`, `tongtien`) VALUES ('3', '3', '3', '2021/3/3', '32323');
-- Table Chitiet--
-- Query 2 Hiển thị thông tin của các cản phẩm có số lượng > 20 và có đơn giá < 1.000.000 VNĐ--
select * 
from sanpham_dochoi
where soluong < 20 and dongia <  100000;
select * 
from sanpham_dodung
where soluong < 20 and dongia <  100000;
select * 
from sanpham_sach
where soluong < 20 and dongia <  100000;
-- update ---
UPDATE `db_nn`.`khachhang` SET `ngaysinh` = '1985/2/2' WHERE (`id` = '1');
UPDATE `db_nn`.`khachhang` SET `ngaysinh` = '1999/1/1' WHERE (`id` = '2');
UPDATE `db_nn`.`khachhang` SET `ngaysinh` = '1978/1/1' WHERE (`id` = '3');
-- Query 3 Hiển thị thông tin của những Khách hàng có độ tuổi >16 tuổi và < 30 tuổi có địa chỉ ở Đà Nẵng hoặc những Khách hàng có độ tuổi > 40 tuổi và có địa chỉ ở Quảng Nam
SELECT * FROM khachhang as tuoi
WHERE ROUND(DATEDIFF(now() , tuoi.ngaysinh)/365) BETWEEN 16 and 30
  AND ROUND(DATEDIFF(now() , tuoi.NgaySinh)/365) = 40;
-- Query 4
select * from donhang inner join khachhang
where donhang.id_kh = khachhang.id and  year(ngaymua) = 2020;

-- Query 5
select * from donhang inner join khachhang
where donhang.id_kh = khachhang.id and month(ngaymua)=4 and year(ngaymua) =2020;
-- Query 6
SELECT * FROM khachhang
WHERE (TenKhachHang LIKE "K%" or TenKhachHang LIKE "H%" or TenKhachHang LIKE "L%") and LENGTH(TenKhachHang) >= 15;

-- Query 7
SELECT TenKhachHang FROM khachhang
GROUP BY TenKhachHang;




